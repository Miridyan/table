use std::env;

use super::Interface;
use super::style::{Theme, Variant};

pub struct Application {
    pub interface: Box<Interface>,
    pub theme: Variant,
}

impl Application {
    pub fn init() -> Application {
        env::set_var("GTK_CSD", "0");
        if super::gtk::init().is_err() {
            panic!("Failed to initialize gtk");
        }

        let interface = Interface::create();

        Application {
            interface: Box::new(interface),
            theme: Variant::Darker,
        }
    }

    pub fn exec(&self) {
        Theme::update_theme(&self.theme);
        self.interface.finalize();

        super::gtk::main();
    }

    pub fn get_interface(&self) -> &Box<Interface> {
        &self.interface
    }
}
