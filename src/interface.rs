use gtk::{MenuItem, MenuItemExt};

use super::style::{Theme, Variant};
use super::dialogs::Dialogs;
use super::main_window::MainWindow;


#[derive(Clone, Debug)]
pub struct Interface {
    pub window: MainWindow,
}

impl Interface {
    pub fn create() -> Interface {
        let window = MainWindow::init();

        {
            let parent = window.window.clone();
            window.get_object::<MenuItem>("preferences-item").unwrap()
                .connect_activate(move |_| {
                    Dialogs::create_prefs(&parent);
                });
        }

        {
            let parent = window.window.clone();
            window.get_object::<MenuItem>("debug-item").unwrap()
                .connect_activate(move |_| {
                    Dialogs::create_debug(&parent);
                });
        }

        {
            let parent = window.window.clone();
            window.get_object::<MenuItem>("open-item").unwrap()
                .connect_activate(move |_| {
                    println!("{:?}", Dialogs::open_file(&parent));
                });
        }

        {
            let parent = window.window.clone();
            window.get_object::<MenuItem>("save-as-item").unwrap()
                .connect_activate(move |_| {
                    println!("{:?}", Dialogs::save_file_as(&parent));
                });
        }

        // {
        //     let parent = window.window.clone();
        //     let mut canvas = window.canvas.clone();
        //     window.get_object::<MenuItem>("reload-theme-item").unwrap()
        //         .connect_activate(move |_| {
        //             canvas.update_bg(Variant::Lighter);
        //             Theme::update_theme(&Variant::Lighter);
        //         });
        // }

        Interface {
            window: window,
        }
    }

    pub fn finalize(&self) {
        self.window.create();
    }
}

unsafe impl Send for Interface {}

unsafe impl Sync for Interface {}
