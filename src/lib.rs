extern crate gdk;
extern crate gtk;

mod style;
mod canvas;
mod config;
mod dialogs;
mod gl_utils;
mod interface;
mod main_window;
mod application;

use gl_utils::GlUtils as GlUtils;
use interface::Interface as Interface;

pub use application::Application as Application;

pub mod gl {
    include!(concat!(env!("OUT_DIR"), "/gl_bindings.rs"));
}
