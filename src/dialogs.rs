use std::path::PathBuf;

use gtk::{Builder, BuilderExt,
          DialogExt,
          FileChooserAction,
          FileChooserDialog, FileChooserExt,
          Inhibit,
          ResponseType,
          Widget, WidgetExt,
          Window, WindowExt};


pub struct Dialogs;

impl Dialogs {
    pub fn create_prefs(parent: &Window) {
        let builder = Builder::new_from_string(include_str!("../res/layout/prefs.glade"));
        let window= builder.get_object::<Window>("prefs-window").unwrap();

        window.set_transient_for(parent);
        window.connect_delete_event(|prefs, _| {
                prefs.destroy();
                Inhibit(false)
            });
        window.show_all();
    }

    pub fn create_debug(parent: &Window) {
        let builder = Builder::new_from_string(include_str!("../res/layout/console.glade"));
        let window = builder.get_object::<Window>("console").unwrap();

        window.set_transient_for(parent);
        window.connect_delete_event(|console, _| {
                console.destroy();
                Inhibit(false)
            });
        window.show_all();
    }

    pub fn open_file(parent: &Window) -> Option<PathBuf> {
        let dialog = FileChooserDialog::new(
                Some("Open File"),
                Some(parent),
                FileChooserAction::Open,
            );

        dialog.set_modal(true);
        dialog.set_transient_for(parent);
        dialog.connect_delete_event(|d, _| {
                d.destroy();
                Inhibit(false)
            });

        dialog.add_button("Open", -3);
        dialog.add_button("Cancel", -6);

        match dialog.run() {
            -3  => {
                let ret = dialog.get_filename();
                dialog.destroy();
                ret
            },
            _ => {
                dialog.destroy();
                None
            }
        }
    }

    pub fn save_file_as(parent: &Window) -> Option<PathBuf> {
        let dialog = FileChooserDialog::new(
                Some("Open File"),
                Some(parent),
                FileChooserAction::Save,
            );

        dialog.set_modal(true);
        dialog.set_transient_for(parent);
        dialog.set_do_overwrite_confirmation(true);
        dialog.connect_delete_event(|d, _| {
                d.destroy();
                Inhibit(false)
            });

        dialog.add_button("Save", -3);
        dialog.add_button("Cancel", -6);

        match dialog.run() {
            -3  => {
                let ret = dialog.get_filename();
                dialog.destroy();
                ret
            },
            _ => {
                dialog.destroy();
                None
            },
        }
    }
}
