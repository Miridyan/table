#![windows_subsystem = "windows"]

extern crate table;

use table::Application;

fn main() {
    let app = Application::init();

    app.exec();
}
