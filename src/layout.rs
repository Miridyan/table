// use gtk::{Builder, BuilderExt,
//           IsA};
//
//
// pub struct Layout {
//     builder: Builder,
// }
//
// impl Layout {
//     pub fn create() -> Layout {
//         let builder = Builder::new();
//
//         builder.add_from_string(include_str!("../res/layout/window.glade"));
//         builder.add_from_string(include_str!("../res/layout/console.glade"));
//         builder.add_from_string(include_str!("../res/layout/prefs.glade"));
//
//         Layout {
//             builder: builder,
//         }
//     }
//
//     pub fn get_object<T: IsA<Object>>(self, object: &str) -> Option<T> {
//         self.builder.clone().get_object(object)
//     }
// }
